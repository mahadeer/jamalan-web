
/**
 * Module dependencies.
 */

var express = require('express');
var passport = require('./auth');
var posts = require('./posts');
var routes = require('./routes');
var data = require('./data');
var model = require('./data/model');
var user = require('./data/user');
var http = require('http');
var path = require('path');
var request = require('request');
var email = require('./email');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.logger('dev'));
app.use(express.cookieParser());
app.use(express.session({
    secret: 'Ix8nZ27TCMUq1LsJ1l91V3a4'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.bodyParser());
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req,res){
    res.redirect('/404');
});

email.gmail();

//404 Route
app.get('/404',function(req,res){
    res.render('404',{
        title: "வருந்துகிறோம், இந்த வலைப்பதிவில் நீங்கள் தேடும் பக்கம் இல்லை."
    });
});

//Post Creation Routes
app.get('/create/post',isAdmin,routes.posts);

app.post('/create/post', isAdmin, function(req,res) {
    console.log('Post Creation Called!!');
    if(posts.addPost(req.body.title, req.body.htmlcontent, req.body.category)) {
        res.redirect('/');
    } else {
        console.log('Error');
    }

});

//Post Editing routes
app.get('/edit/post/:id', isAdmin, function(req,res){
    posts.editPost(req,res,req.params.id);
});

app.post('/edit/post/:id', isAdmin, function(req,res){
    posts.updatePost(req.body.title, req.body.htmlcontent, req.body.category, req.params.id,res);
});

//Feedback Route
app.post('/feedback', function(req,res){
    posts.addFeedback(req, res);
});

//General Routes
app.get('/', routes.index);

app.post('/post/comment/:id',isLoggedIn, function(req,res){
    posts.addComment(req, res);
});

app.get('/list/posts', isAdmin, function(req,res){
    posts.getPostsList(req,res);
});

app.get('/list/feedbacks',isAdmin,function(req,res){

});

app.get('/get/post/:id/comments',function(req,res){
    posts.getComments(req,res);
});

app.get('/get/posts', function (req, res){
    posts.getPosts(req,res);
});

app.get('/get/post/:id', function(req,res,next){
    posts.getPost(req,res,req.params.id);
});

//Category Route Section
app.get('/category/:cat', function (req, res){
    posts.getCategories(req,res, req.params.cat);
});

//Likes Section
app.get('/post/:id/like',isLoggedIn, function(req,res){
    posts.likePost(req,res, req.params.id);
});
app.get('/getUserImage', function(req,res){
    if(!req.isAuthenticated()) {
        res.json('null')
    } else {
        res.json(req.session.passport.user.image);
    }
});

app.get('/comment/:id/like',isLoggedIn, function(req,res){
    posts.likeComment(req,res, req.params.id);
});


//Authentication Routes
app.get('/isAuth', function(req,res) {
    if (!req.isAuthenticated()) {
        res.json({'role': 'login'});
    } else {
        if(req.session.passport.user.id === '101admin'){
            res.json({'role': 'admin'});
        } else {
            res.json({'role': 'user'});
        }
    }
});

app.get('/auth', routes.auth);

app.post('/auth', passport.authenticate('local',{
        successRedirect: '/logged/redirect',
        failureRedirect: '/auth'
    })
);

app.get('/logged/redirect',isLoggedIn,function(req,res) {
    if (req.session.passport.user.id === '101admin'){
        res.redirect('/admin/dashboard');
    }else {
        res.redirect(req.session.returnTo);
    }
});
        //res.redirect('/admin/dashboard');
//Admin Dashboard
app.get('/admin/dashboard',isAdmin,routes.dashboard);

app.get('/logout', function(req, res) {
    req.logout();
    res.redirect(req.session.returnTo);
});

app.get('/auth/google', passport.authenticate(
    'google', {
        scope : ['profile', 'email']
    }
));

// the callback after google has authenticated the user
app.get('/auth/google/return',
    passport.authenticate('google', {
        successRedirect: '/logged/redirect',
        failureRedirect: '/auth'
    })
);

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();
    }

    // if they aren't redirect them to the home page
    res.redirect('/auth');
}
function isAdmin(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        if(req.session.passport.user.id === '101admin')
            return next();
    }

    // if they aren't redirect them to the home page
    res.redirect('/auth');
}

http.createServer(app).listen(app.get('port'), function(){
  console.log('Jamalan server listening on port ' + app.get('port'));
});
