# Jamalan Web README

### Recent Changes
* Updates blog post automatically when a new post is added on jamalan.tamil.blogspot.com 
* E-Mail Support added 

### Pending Works

* Dashboard for analyctics 
* Feedback viewer 
* Automatic Email Message 

### Version
> 1.3 chnagelogs provided, see above.

### Tech

Jamalan Web uses a number of open source projects to work properly:

* [AngularJS] - HTML enhanced for web apps!
* [Sublime Text Editor 3] - awesome open source text editor
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [jQuery] - duh

**Thanks for the support!**

[Sublime Text Editor 3]:http://www.sublimetext.com/
[node.js]:http://nodejs.org
[Twitter Bootstrap]:http://twitter.github.com/bootstrap/
[jQuery]:http://jquery.com
[express]:http://expressjs.com
[AngularJS]:http://angularjs.org