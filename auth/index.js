var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var configAuth = require('./config');

var mongoose = require('../data').db;
var User = mongoose.model('User');

passport.use(new LocalStrategy(
    function(username, password, done){
        var user = new User();
        if (username === 'admin' && password === 'admin1234#') {
            user.id    = '101admin';
            user.token = 'token';
            user.displayName  = 'admin';
            user.email = 'jamalan.web@gmail.com'; // pull the first email
            user.name.givenName = 'Jamalan';
            user.name.familyName = '';
            user.name.middleName = '';
            return done(null, user );
        }
        return done(null, false);
    }
));
// used to serialize the user for the session
passport.serializeUser(function(user, done) {
    done(null, user);
});

// used to deserialize the user
passport.deserializeUser(function(user, done) {
    done(null, user);
});

// code for login (use('local-login', new LocalStategy))
// code for signup (use('local-signup', new LocalStategy))
// code for facebook (use('facebook', new FacebookStrategy))
// code for twitter (use('twitter', new TwitterStrategy))

// =========================================================================
// GOOGLE ==================================================================
// =========================================================================
passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL

    },
    function(token, refreshToken, profile, done) {

        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function() {
            User.findOne({ 'id' : profile.id }, function(err, user) {
                if (err)
                    return done(err);

                if (user) {

                    // if a user is found, log them in
                    return done(null, user);
                } else {
                    // if the user isnt in our database, create a new user
                    var newUser          = new User();
                    // set all of the relevant information
                    newUser.id    = profile.id;
                    newUser.token = token;
                    newUser.displayName  = profile.displayName;
                    newUser.email = profile.emails[0].value; // pull the first email
                    newUser.name.givenName = profile.name.givenName;
                    newUser.name.familyName = profile.name.familyName;
                    newUser.name.middleName = profile.name.middleName;
                    newUser.image = profile._json.picture;

                    // save the user
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
//            if (profile) {
//
//                // if a user is found, log them in
//                return done(null, {
//                    id    : profile.id,
//                    token : token,
//
//               username  : profile.displayName,
//                    email : profile.emails[0].value // pull the first email
//                });
//            }
        });

    }));

module.exports = passport;