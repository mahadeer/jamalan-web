var appControllers = angular.module('appControllers',[]);

appControllers.controller('analycticsCtrl', ['$scope', function($scope) {
    $scope.title = 'Analyctics'
}]);
appControllers.controller('feedbacksCtrl', ['$scope', function($scope) {
    $scope.title = 'Feedback'
}]);
appControllers.controller('managepostsCtrl', ['$scope','$http', function($scope,$http) {
    $scope.title = 'Manage Posts';
    $http.get('/list/posts').
        success(function(data){
            $scope.datas = angular.fromJson(data);
        });
}]);
appControllers.controller('overviewCtrl', ['$scope', function($scope) {
    $scope.title = 'Overview'
}]);
appControllers.controller('reportsCtrl', ['$scope', function($scope) {
    $scope.title = 'Reports'
}]);