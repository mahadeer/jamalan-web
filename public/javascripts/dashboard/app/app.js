var app = angular.module('app',['ngRoute','appControllers']);

app.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl: '../pages/dashboard/overview.html',
            controller: 'overviewCtrl'
        })
        .when('/analyctics',{
            templateUrl : '../pages/dashboard/analyctics.html',
            controller  : 'analycticsCtrl'
        })
        .when('/feedbacks',{
            templateUrl : '../pages/dashboard/feedbacks.html',
            controller  : 'feedbacksCtrl'
        })
        .when('/manageposts',{
            templateUrl : '../pages/dashboard/manageposts.html',
            controller  : 'managepostsCtrl'
        })
        .when('/reports',{
            templateUrl : '../pages/dashboard/reports.html',
            controller  : 'reportsCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);