var myAppControllers = angular.module('myAppControllers',['ui.bootstrap']);

myAppControllers.controller('Controller.Home',['$scope','$http','$sce', function($scope,$http,$sce) {
    $http.get('/get/posts').
        success(function(data, status, headers, config) {
            $scope.posts = angular.fromJson(data);
        }).
        error(function(data, status, headers, config) {

        });
    $scope.getShortContent = function(html_code) {

        var content = getPTags(html_code);
        return $sce.trustAsHtml(content);
    };
    $scope.getImage = function(html_code) {
        var imageTag = parseImageTag(html_code,'<img','/>');
        return $sce.trustAsHtml(imageTag);
    };

    $scope.getComments = function(postComments) {
        var len = 0;
        if(postComments != null) {
            len = postComments.length;
        }
        var html = '<label style="font-size: 18px;color: #000000; padding-right:30px; padding-left: 10px"> ' + len + ' Comments</label>';
        return $sce.trustAsHtml(html);
    };

}]);

myAppControllers.controller('Controller.Category',['$scope','$http','$sce','$routeParams', function($scope,$http,$sce,$routeParams) {

    var address = '/category/' + $routeParams.cat;
    $scope.label = 'நீங்கள் பார்ப்பது ' + $routeParams.cat + ' தொகுப்புகளை!';
    $http.get(address).
        success(function(data, status, headers, config) {
            $scope.posts = angular.fromJson(data);
        }).
        error(function(data, status, headers, config) {

        });
    $scope.getShortContent = function(html_code) {

        var content = getPTags(html_code);
        return $sce.trustAsHtml(content);
    };
    $scope.getImage = function(html_code) {
        var imageTag = parseImageTag(html_code,'<img','/>');
        return $sce.trustAsHtml(imageTag);
    };

    $scope.getComments = function(postComments) {
        var len = 0;
        if(postComments != null) {
            len = postComments.length;
        }
        var html = '<label style="font-size: 18px;color: #000000; padding-right:30px; padding-left: 10px"> ' + len + ' Comments</label>';
        return $sce.trustAsHtml(html);
    };

}]);

myAppControllers.controller('Controller.Post', ['$scope','$http','$sce','$routeParams','$modal', function($scope,$http,$sce,$routeParams,$modal) {

    var address = '/get/post/' + $routeParams.id;
    $http.get(address).
        success(function(data, status, headers, config) {
            $scope.post = angular.fromJson(data);
            var userImage = null;
            $http.get('/getUserImage').
                success(function(uData){
                    userImage = angular.fromJson(uData);
                    if(data.likes.people != null) {
                        for (var i = 0; i < data.likes.people.length; i++) {
                            if (data.likes.people[i].image === userImage) {
                                removeUserLikes();
                            }
                        }
                    }
                });
        }).
        error(function(data, status, headers, config) {

        });
    $scope.renderHtml = function(html_code) {
        return $sce.trustAsHtml(html_code);
    };

    $scope.validate = function(img, tagId) {
        var userImage;
        $http.get('/getUserImage').
            success(function(uData){
                userImage = angular.fromJson(uData);
                if(img === userImage) {
                    removeUserCommentLikes(tagId);
                }
            });
    };
    $http.get('/isAuth').
        success(function(data, status, headers, config) {
            if(data.role === 'login' ){
                $scope.auth = false;
                $scope.admin = false;
            } else if(data.role === 'admin' ) {
                $scope.auth = true;
                $scope.admin = true;
            } else if(data.role === 'user') {
                $scope.auth = true;
                $scope.admin = false;
            }
        }).
        error(function(data, status, headers, config) {

        });
    var ObjId = '/get/post/' + $routeParams.id + '/comments';
    $http.get(ObjId).
        success(function(data){
            $scope.comments = angular.fromJson(data);
        });

    $scope.showLikes = function(data) {
        var size = 'sm';
        $modal.open({
            templateUrl: 'myLikeModal.html',
            controller: function ($scope, $modalInstance) {
                $scope.done = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.people = data;
            },
            size: size,
            replace: false
        });
    }
}]);

myAppControllers.controller('Controller.About', function($scope){

});

myAppControllers.controller('Controller.ContactUs', function($scope){

});

myAppControllers.controller('Controller.404', function($scope){
    $scope.error = "Sorry, We couldn't find the page requested!!!";
});