var maJamalan = angular.module('maJamalan',['ngRoute','myAppControllers']);

maJamalan.config(['$routeProvider',function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl: '../pages/home.html',
            controller: 'Controller.Home'
        })
        .when('/post/:id',{
            templateUrl : '../pages/post.html',
            controller  : 'Controller.Post'
        })
        .when('/category/:cat',{
            templateUrl : '../pages/category.html',
            controller  : 'Controller.Category'
        })
        .when('/contactus',{
            templateUrl: '../pages/contactus.html',
            controller: 'Controller.ContactUs'
        })
        .when('/about',{
            templateUrl: '../pages/about.html',
            controller: 'Controller.About'
        })
        .when('/books',{
            templateUrl: '../pages/books.html'
        })
        .when('/links',{
            templateUrl: '../pages/links.html'
        })
        .when('/404',{
            templateUrl: '../pages/404.html',
            controller: 'Controller.404'
        })
        .otherwise({
            redirectTo: '/404'
        });
}]);