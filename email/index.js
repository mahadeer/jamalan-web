var MailListener = require('mail-notifier'),
	blogPosts = require('../posts/blogPost');

var listenerOptions = {
	username: 'jamalan.web',
	password: 'jsms1234#',
	host: 'imap.gmail.com',
	port: 993,
	tls: true,
	tlsOptions: { rejectUnauthorized: false },
	mailbox: "INBOX", // mailbox to monitor
	searchFilter: ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
	markSeen: true, // all fetched email willbe marked as seen and not fetched next time
	fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
	mailParserOptions: {streamAttachments: false}, // options to be passed to mailParser lib.
	attachments: false, // download attachments as they are encountered to the project directory
	attachmentOptions: { directory: "attachments/" }
};

// gmail.on('server:connected',function(){
// 	console.log('Gmail server connected successfully!!!');
// });

// gmail.on('error',function(err){
// 	console.log('Gmail server connection error details: ' + err);
// });

// gmail.on('mail', function(mail,seqno,attributes){
// 	console.log('New Email Arrived!!!');
// 	console.log(mail);
// 	console.log(seqno);
// 	console.log(attributes);
// });

// gmail.on('server:disconnected',function(){
// 	console.log('Gmail server unfortunately disconnected!!!');
// });

exports.gmail = function gmail() {
	MailListener(listenerOptions).on('mail',function(mail){
		console.log('New Email Arrived!!!');
		if(mail.from[0].address === 'feedblitz@mail.feedblitz.com'){
			console.log('New Blog Post Updated!!!');
			blogPosts.updateBlogPosts();
		} else if(mail.from[0].address === 'mahadeer2010@gmail.com') {
			console.log('Admin has requested an email to update posts!!!');
		}
	}).start();

	MailListener(listenerOptions).on('error',function(err){
		console.log('Error Occured: ' + err);
	});
};