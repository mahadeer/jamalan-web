var mongoose = require('mongoose');


var db = mongoose.connection;

db.on('error', console.error);

db.once('open',function(){
    console.log('Database Opened');
});

mongoose.connect('mongodb://localhost:27017/jamalan');
require('./user');


exports.db = db;
