var mongoose = require('mongoose');
var userSchema = new mongoose.Schema({
    id: {type : String},
    name: {
        familyName: String,
        givenName: String,
        middleName: String
    },
    displayName: String,
    emails: String,
    image: String,
    token: Object
});

var user = mongoose.model('User',userSchema);