var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
    body: String,
    author: {
        id: {type : String},
        displayName: String,
        email: String,
        image: String
    },
    posted: String,
    likes: {
        total: {type: Number, default: 0},
        people: Array
    },
    post: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' }
});

mongoose.model('Comment', CommentSchema);