var mongoose = require('mongoose');

var postSchema = new mongoose.Schema({
    title: String,
    posted: String,
    content: String,
    category: String,
    likes: {
        total: {type: Number, default: 0},
        people: Array
    },
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
});

var user = mongoose.model('Post',postSchema);