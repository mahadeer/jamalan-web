var mongoose = require('mongoose');

var FeedbackSchema = new mongoose.Schema({
    name: String,
    email: String,
    message: String
});

mongoose.model('Feedback', FeedbackSchema);