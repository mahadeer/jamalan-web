/*
 * GET home page.
 */
var isAdmin = function(req){
    if (!req.isAuthenticated()) {
        return false;
    } else {
        if(req.session.passport.user.id === '101admin'){
            return true;
        } else {
            return false;
        }
    }
};
var isUser = function(req){
    if (!req.isAuthenticated()) {
        return false;
    } else {
        if (req.session.passport.user.id === '101admin'){
            return false;
        } else {
            return true;
        }
    }
};
var getUser = function(req) {
    if (!req.isAuthenticated()) {
        return null;
    } else {
        return req.user.displayName.toString();
    }
};

exports.index = function(req, res){
  res.render('index', {
      title: 'ஜமாலன்',
      isUser: isUser(req),
      isAdmin: isAdmin(req),
      user: getUser(req)
  });
};

exports.posts = function(req,res){
    res.render('post',{
        title: 'Create Blog Posts',
        user: req.user,
        isEditing: false,
        postTitle : null,
        postCategory: null,
        postContent: null,
        id : null
    });
};

exports.auth = function(req, res){
    res.render('login',{title: 'Log in'});
};

exports.dashboard = function(req,res) {
    res.render('dashboard', {
        title: 'Admin Dashboard'
    });
};