var request = require('request'),
    mongoose = require('../data/model').db,
    Post = mongoose.model('Post'),
    BlogPosts = mongoose.model('BlogPosts');

module.exports.updateBlogPosts = function() {
    request('http://jamalantamil.blogspot.in/feeds/posts/default?alt=json&max-results=1', function (error, response, body) {

        if (!error && response.statusCode == 200) {
            var blogData = JSON.parse(body);
            var posts = blogData.feed.entry;
            posts.forEach(function(item){
                var catVal = "";
                if(item.category !== undefined) {
                    catVal = item.category[0].term;
                }
                // Start Inserting in the database
                var postId = item.id.$t;
                BlogPosts.find({postId: postId}, function(err,data){
                    if(!err) {
                        if(data.length < 1) {
                            CreateData(item,catVal);
                            AddToList(item.id.$t);
                        }
                    }
                });
                //
//                var thisPost = {
//                    postId: item.id.$t,
//                    title: item.title.$t,
//                    category: catVal,
//                    content: item.content.$t
//                };
//                jsonData.push(thisPost);
            });
        }
    })
};

function CreateData(blogpost,category) {
    var post = new Post();
    post.title = blogpost.title.$t;
    var d = new Date();
    post.posted = ("00" + (d.getMonth() + 1)).slice(-2) + "/" + ("00" + d.getDate()).slice(-2) + "/" + d.getFullYear() + " " + ("00" + d.getHours()).slice(-2) + ":" + ("00" + d.getMinutes()).slice(-2) + ":" + ("00" + d.getSeconds()).slice(-2);
    post.content = ParseContent(blogpost.content.$t);
    post.comments = null;
    post.likes.total = 0;
    post.likes.people = null;
    post.category = category;

    post.save(function(err){
        if (err)
            return false;
        console.log('Post Successfully saved!!!');
    });
}

function ParseContent(content) {
    var tempContent = content.replace(/<p align="justify">/g, '<p>');
    var pContent = tempContent.replace('<img', '<img class="image"');
    return pContent;
}

function AddToList(id) {
    var blogPost = new BlogPosts();
    blogPost.postId = id;
    blogPost.save(function(err){
        if(err)
            console.log(err);
        console.log('Added Blog Post to list');
    });
}