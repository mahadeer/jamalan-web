var mongoose = require('../data/model').db;
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var Feedback = mongoose.model('Feedback');

module.exports.addPost = function(title, content, category){
    var post = new Post();
    post.title = title;
    var d = new Date();
    post.posted = ("00" + (d.getMonth() + 1)).slice(-2) + "/" + ("00" + d.getDate()).slice(-2) + "/" + d.getFullYear() + " " + ("00" + d.getHours()).slice(-2) + ":" + ("00" + d.getMinutes()).slice(-2) + ":" + ("00" + d.getSeconds()).slice(-2);
    post.content = content;
    post.comments = null;
    post.likes.total = 0;
    post.likes.people = null;
    post.category = category;

    post.save(function(err){
        if (err)
            return false;
        console.log('Post Successfully saved!!!');
    });
    return true;
};

module.exports.getPosts = function(req, res){
    req.session.returnTo = '/';
    Post.find().sort({'_id': 'desc'}).limit(5).exec(function(err,doc) {
        return res.json(doc);
    });
};

module.exports.getPost = function(req,res,id) {
    req.session.returnTo = '/#/post/' + id;
    Post.findById(id,function(err,post){
        if(err) return res.json('Post Error');
        if(post) return res.json(post);
    });
};

module.exports.getCategories = function(req,res,id) {
    req.session.returnTo = '/#/category/' + id;
    Post.find({'category' : id}).sort({'category': 'desc'}).limit(5).exec(function(err,doc) {
        console.log(doc);
        return res.json(doc);
    });
};

module.exports.getComments = function(req,res){
    Comment.find({post: req.params.id}).sort({'_id':'desc'}).exec(function(err,comments){
        res.json(comments);
    });
};

module.exports.addComment = function(req,res) {
    var comment = new Comment();
    comment.body = req.body.comment;
    comment.post = req.params.id;
    var d = new Date();
    comment.posted = ("00" + (d.getMonth() + 1)).slice(-2) + "/" + ("00" + d.getDate()).slice(-2) + "/" + d.getFullYear() + " " + ("00" + d.getHours()).slice(-2) + ":" + ("00" + d.getMinutes()).slice(-2) + ":" + ("00" + d.getSeconds()).slice(-2);
    comment.author.id = req.user.id;
    comment.author.displayName = req.user.displayName;
    comment.author.email = req.user.email;
    comment.author.image = req.user.image;

    //likes section
    comment.likes.total = 0;
    comment.likes.people = null;

    comment.save(function(err, comment){
        Post.findById(req.params.id,function(err,post){
            if(post) {
                if (post.comments != null) {
                    post.comments.push(comment);
                } else {
                    post.comments = comment;
                }
                post.save(function(err){
                });
            }
            return true;
        });
        return true;
    });
    var redirectId = '/#/post/' + req.params.id;
    res.redirect(redirectId);
};

module.exports.likePost = function(req,res,id) {
    Post.findById(id,function(err,post){
        if(err) return res.json('Post Error');
        if(post) {
            post.likes.total += 1;
            var thisGuy = {
                name : req.user.displayName,
                image : req.user.image
            };
            if (post.likes.people != null) {
                post.likes.people.push(thisGuy);
            } else {
                post.likes.people = thisGuy;
            }

            post.save(function(err){
                if (err)
                    return false;
                console.log('Post Likes Successfully saved!!!');

            });
        }
    });
    var redirectId = '/#/post/' + id;
    res.redirect(redirectId);
};

module.exports.editPost = function(req,res,id) {
    Post.findById(id,function(err,post){
        if(err) return res.json('Post Error');
        if(post) {
            res.render('post',{
                title: 'Edit Post',
                user: req.user,
                isEditing: true,
                postTitle : post.title,
                postCategory: post.category,
                postContent: post.content,
                id : id
            });
        }
    });
};

module.exports.updatePost = function(title, content, category, id, res){
    var d = new Date();
    var posted = ("00" + (d.getMonth() + 1)).slice(-2) + "/" + ("00" + d.getDate()).slice(-2) + "/" + d.getFullYear() + " " + ("00" + d.getHours()).slice(-2) + ":" + ("00" + d.getMinutes()).slice(-2) + ":" + ("00" + d.getSeconds()).slice(-2);
    Post.update({ _id: id }, { $set: {
        title: title,
        content: content,
        category: category,
        posted: posted
    }}, success);
    function success() {
        console.log('Post Updated saved!!!');
        res.redirect('/admin/dashboard');
    }
};


module.exports.likeComment = function(req,res,id) {
    var postId;
    Comment.findById(id,function(err,comment){
        postId = comment.post;
        console.log(comment);
        if(err) return res.json('Post Error');
        if(comment) {
            comment.likes.total += 1;
            var thisGuy = {
                name : req.user.displayName,
                image : req.user.image
            };
            if (comment.likes.people != null) {
                comment.likes.people.push(thisGuy);
            } else {
                comment.likes.people = thisGuy;
            }
            comment.save(function(err){
                if (err)
                    return false;
                console.log('Comment Likes Successfully saved!!!');
                var redirectId = '/#/post/' + postId;
                res.redirect(redirectId);
            });
        }
    });
};

module.exports.addFeedback = function(req,res){
    var feedback = new Feedback();
    feedback.name = req.body.name;
    feedback.email = req.body.email;
    feedback.message = req.body.message;
    feedback.save(function(err){
        if(err)
            console.log('Error Saving Feedback');
    });
    res.end();
    res.redirect('/#contactus');
};

module.exports.getPostsList = function(req,res){
    var list = [];
    Post.find().sort({'_id': 'desc'}).exec(function(err,doc) {
        doc.forEach(function(post){
            list.push({
                _id: post._id,
                title: post.title,
                category: post.category
            });
        });
        res.json(list);
    });
};

